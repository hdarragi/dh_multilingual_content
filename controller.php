<?php     
/**
 * @author Hamed Darragi <darragihamed@gmail.com>
 * http://darragihamed.wordpress.com/
 */
defined('C5_EXECUTE') or die(_("Access Denied."));

class DhMultilingualContentPackage extends Package {

    protected $pkgHandle = 'dh_multilingual_content';
    protected $appVersionRequired = '5.6.3.1';
    protected $pkgVersion = '1.0.0';
    
    public function getPackageDescription() {
    	return t("Multilingual HTML/WYSIWYG Editor Content.");
    }

    public function getPackageName() {
    	return t("Multilingual Content");
    }
    
    public function install() {
        $ml = Package::getByHandle('multilingual');
        if (!is_object($ml)) {
            throw new Exception(t('You must have the %s add-on installed in order to use this add-on!', t('Internationalization')));
        }
    	$pkg = parent::install();
	    // install block
        BlockType::installBlockTypeFromPackage('dh_multilingual_content', $pkg);
    }   

}
?>
