<?php

defined('C5_EXECUTE') or die('Access Denied.');

class LanguageListHelper {

    public function getAllLanguages() {
        $db = Loader::db();
        $ifh = Loader::helper('interface/flag', 'multilingual');
        
        $languages = array();
        $query = 'SELECT msLanguage, msIcon, msLocale FROM MultilingualSections ORDER BY msLocale';

        $result = $db->Execute($query);
        while ($row = $result->FetchRow()) {
            $zendLocale = new Zend_Locale($row['msLocale']);
            $languages[] = array(
                'language' => $row['msLanguage'],
                'locale' => $row['msLocale'],
                'icon' => $ifh->getFlagIcon($row['msIcon']),
                'zend_locale' => $zendLocale,
                'translation' => $zendLocale->getTranslation($zendLocale->getLanguage(), 'language', $zendLocale->getLanguage())
            );
        }
        return $languages;
    }

}
