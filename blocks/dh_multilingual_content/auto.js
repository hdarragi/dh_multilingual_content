    
    ccmValidateBlockForm = function() { 
        if($('.dh-advanced-content-editor').length == 0){
            ccm_addError(ccm_t('cannot-be-added'));
            return false;
        }
        return  true;
    }
    
    mlContentTabSetup = function() {
        $('ul#ccm-ml-content-tabs li a').each( function(num,el){ 
            el.onclick=function(){
                var pane=this.id.replace('ccm-ml-content-tab-','');
                mlContentShowPane(pane);
            }
        });		
    }

    mlContentShowPane = function (pane){
        $('ul#ccm-ml-content-tabs li').each(function(num,el){ $(el).removeClass('ccm-nav-active') });
        $(document.getElementById('ccm-ml-content-tab-'+pane).parentNode).addClass('ccm-nav-active');
        $('div.ccm-mlContentPane').each(function(num,el){ el.style.display='none'; });
        $('#ccm-mlContentPane-'+pane).css('display','block');
    }

$(function() {	
        mlContentTabSetup();		
});

