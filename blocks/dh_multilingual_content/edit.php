<?php
defined('C5_EXECUTE') or die("Access Denied.");
//$replaceOnUnload = 1;
$c = Page::getCurrentPage();
$th = Loader::helper('text');
 $globalArea  = Area::get($c,$_REQUEST['arHandle'],true);
 if(isset($_REQUEST['cID'])){
     $stack = Stack::getByID($_REQUEST['cID']);
 }
?>
<?php if(!$languages || empty($languages)):?>
<ul class="ccm-error">
    <li><?php echo t('This block is designed for multilingual sites');?>,<br/>
        <?php echo t('please add languages to your site');?>.
    </li>
</ul>
<?php elseif((!$globalArea || !$globalArea->arIsGlobal) && !$c->isMasterCollection() && !$stack):?>
<ul class="ccm-error">
    <li><?php echo t('This block is designed to be added in a sitewide area');?>,<br/>
        <?php echo t('otherwise you may use standard content block');?>.
    </li>
</ul>
<?php else:?>
<?php $theme = $c->getCollectionThemeObject(); ?>
<?php $this->inc('editor_config.php', array('theme' => $theme)); ?> 
<style type="text/css">
    div.ccm-mlContentPane-ln-icon {
        position: absolute;
        right: 25px;
        top: 7px;
    }
    .ccm-dialog-tabs {float: none;}
</style>
<!-- Begin: Tabs -->
<ul id="ccm-ml-content-tabs" class="ccm-dialog-tabs">
    <?php foreach ($languages as $language): ?>
    <li class="<?php echo $language['locale'] == $CURRENT_LOCALE?"ccm-nav-active":""; ?>"><a id="ccm-ml-content-tab-<?php echo $language['locale']; ?>" href="javascript:void(0);"><?php echo $language['icon']?> <?php echo ucfirst($language['translation']);?></a></li>
    <?php endforeach; ?>
</ul>
<!-- End: Tabs -->
<div class="ccm-ui">
     <?php foreach ($languages as $language): ?> 
	<div class="ccm-mlContentPane" id="ccm-mlContentPane-<?php echo $language['locale']; ?>" style="<?php echo $language['locale'] != $CURRENT_LOCALE?"clear: both;display: none":""; ?>">
	    <div class="clearfix" style="width:99%;position: relative;">
                <div class="ccm-mlContentPane-ln-icon"><?php echo $language['icon']?></div>
                <?php $this->inc('editor_init.php'); ?>
                <div style="text-align: center" id="ccm-editor-pane-<?php echo $language['locale']; ?>">
                    <?php echo $form->textarea('content['.$language['locale'].']',$th->specialchars($controller->getContentEditMode($language['locale'])) ,array("class" => "advancedEditor ccm-advanced-editor dh-advanced-content-editor")); ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>