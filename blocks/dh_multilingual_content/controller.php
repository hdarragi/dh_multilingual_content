<?php defined('C5_EXECUTE') or die("Access Denied.");
/**
 * The controller for the multinlingual content block.
 * it is an extension of the core content block
 * @package Blocks
 * @subpackage Multilingual Content
 * @author Hamed Darragi <darragihamed@gmail.com>
 * @copyright  Copyright (c) 2003-2014 Concrete5. (http://www.concrete5.org)
 * @license    http://www.concrete5.org/license/     MIT License
 *
 */

class DhMultilingualContentBlockController extends BlockController {
		
        protected $btTable = 'btDhMultilingualContent';
        protected $btInterfaceWidth = "600";
        protected $btInterfaceHeight = "465";
        protected $btCacheBlockOutput = true;
        protected $btCacheBlockOutputLifetime = 0; //until manually updated or cleared
        
        private $CURRENT_LOCALE;
        
        public function __construct($obj = null) {
            parent::__construct($obj);
            $sh = Loader::helper('section','multilingual');
            $this->CURRENT_LOCALE = $sh->getLocale();
        }
        
        public function getBlockTypeDescription() {
            return t("Multilingual HTML/WYSIWYG Editor Content.");
        }

        public function getBlockTypeName() {
            return t("Multilingual Content");
        }
        
        function composer(){
            $this->setVariables();
        }
        
        function add(){
            $this->setVariables();
        }
        
        function edit(){
            $this->setVariables();
        }
        
        private function setVariables(){
            $lnlisth = Loader::helper("language_list","dh_multilingual_content");
            $languages = $lnlisth->getAllLanguages();
            $this->set('languages', $languages);
            $this->set('CURRENT_LOCALE',$this->CURRENT_LOCALE);
        }
        
        public function getJavaScriptStrings() {
            return array(
                    'cannot-be-added' => t('Sorry, the block cannot be added.'),
            );
	}
        
        function getContent($locale = '') {
            $locale = $locale != ''?$locale:  $this->CURRENT_LOCALE;
            $content = unserialize($this->content);
            return Loader::helper('content')->translateFrom($content[$locale]);			
        }

        public function getSearchableContent(){
            return $this->getContent();
        }

        function getContentEditMode($locale = '') {
            $locale = $locale != ''?$locale:  $this->CURRENT_LOCALE;
            $content = $this->getContent($locale);
            return Loader::helper('content')->translateFromEditMode($content);			
        }
        
        public function getImportData($blockNode) {
		$args = array();
                $lnlisth = Loader::helper("language_list","dh_multilingual_content");
                $languages = $lnlisth->getAllLanguages();
                $contentHelper = Loader::helper('content');
                foreach ($languages as $ln){
                    $langNode = $blockNode->data->record->content->{$ln};
                    if($langNode){
                        $args['content'][$ln] = $contentHelper->import($langNode);
                    }else{
                        $args['content'][$ln] = "";
                    }
                }
		return $args;
	}

	public function export(SimpleXMLElement $blockNode) {
            $data = $blockNode->addChild('data');
            $data->addAttribute('table', $this->btTable);
            $record = $data->addChild('record');
            $cnode = $record->addChild('content');
            
            $lnlisth = Loader::helper("language_list","dh_multilingual_content");
            $languages = $lnlisth->getAllLanguages();
            
            $contentHelper = Loader::helper('content');
            foreach ($languages as $ln){
                $lnNodeRec = $cnode->addChild($ln);
                $lnNode = dom_import_simplexml($lnNodeRec);
                $no = $lnNode->ownerDocument;
                $lnNode->appendChild($no->createCDataSection($contentHelper->export($this->getContent($ln))));
            }
	}
        
        function save($args) {
            $contents = array();
            $contentHelper = Loader::helper('content');
            foreach ($args['content'] as $locale => $content){
                $contents[$locale] = $contentHelper->translateTo($content);
            }
            $args['content'] = serialize($contents);
            parent::save($args);
        }

}